<?
class Cookie_Consent_Banner_Class{

    public $ccb_countries=Array(
        'UA' => 'Ukraine',
        'US' => 'USA',
        'NL' => 'Netherlands',
        'DE' => 'Germany',
        'PL' => 'Poland'
    );

    public $file;

    function __construct($file) {

        $this->file=$file;
        register_activation_hook( $file, array($this, 'cbb_install'));
        register_deactivation_hook( $file, array($this, 'cbb_uninstall'));

        add_action( 'admin_menu', array( $this, 'ccb_options_page')  );
        add_action( 'admin_enqueue_scripts', array( $this, 'ccb_admin_scripts_and_styles'));
        add_action( 'wp_enqueue_scripts', array( $this, 'ccb_scripts_and_styles') );

        add_action( 'wp_ajax_ccb_add_country_fields', array( $this, 'ccb_add_country_fields'));
        add_action( 'wp_footer', array( $this, 'cbb_add_consent_baner') );

        
        add_shortcode( 'cbb_term_link', array($this, 'cbb_term_link_shortcode') );

        add_filter( 'plugin_action_links_'.plugin_basename($file), array( $this, 'ccb_settings_link'));  
    }

    function cbb_install() {
        add_option( 'ccb_content', 'Our Website uses cookies to improve your experience. If you want to learn more or withdraw your consent, please refer to [cbb_term_link]the cookie policy[/cbb_term_link]' );
        add_option( 'ccb_bg_color', '#1e73be' );
        add_option( 'ccb_button_color', '#eeee22' );  
    }

    function cbb_uninstall() {
        delete_option( 'ccb_content' );
        delete_option( 'ccb_custom_countries' );
        delete_option( 'ccb_term_link' );
        delete_option( 'ccb_icon' );
        delete_option( 'ccb_bg_color' );
        delete_option( 'ccb_button_color' );  
    }

    function ccb_options_page() {
        add_menu_page(
            'Cookie consent',
            'Cookie consent',
            'manage_options',
            'cookie-consent-banner',
            array( $this, 'cookie_consent_banner_settings'),
            plugin_dir_url(__FILE__) . '../assets/images/icon_ccb.png',
            150
        );
    }
    
    function ccb_admin_scripts_and_styles() {
        //scripts
     
        wp_register_script('media-uploader', plugins_url('assets/js/ccb_admin_scripts.js' , $this->file ), array('jquery'));
        wp_enqueue_media();
        wp_enqueue_script('wp-color-picker'); 
        wp_enqueue_script('media-uploader');
    
        //styles
        wp_enqueue_style( 'wp-color-picker', false, true ); 
        wp_enqueue_style( 'ccb-style-admin', plugins_url('assets/css/ccb_admin_style.css', $this->file ) );
    }

    function ccb_scripts_and_styles() {
        //scripts
     
        wp_register_script('ccb-scripts', plugins_url('assets/js/ccb_scripts.js' , $this->file ), array('jquery'));
        wp_enqueue_script('ccb-scripts');
    
        //styles
        wp_enqueue_style( 'ccb-style', plugins_url('assets/css/ccb_style.css', $this->file) );
    }

    function ccb_settings_link( $links ) {
        $links[] = '<a href="' .
            admin_url( 'admin.php?page=cookie-consent-banner' ) .
            '">' . __('Settings') . '</a>';
        return $links;
    }

    
    function ccb_update_options($post)
    {
        if(isset($post)){
            foreach($post as $key => $value)
            {
                if ($key=='ccb_country_message'):
                    $custom_country_messages=$value;
                elseif ($key=='ccb_country_name'):
                    $custom_country_name=$value;
                else:
                    update_option($key, stripslashes($value));
                endif;
            }
    
            if (count($custom_country_name)>0):
                $custom_country_option=array();
                foreach($custom_country_name as $i => $country):
                    $custom_country_option[$country]=stripslashes($custom_country_messages[$i]);
                endforeach;
    
                update_option('ccb_custom_countries', $custom_country_option);
    
            endif;
            echo '</pre>';
        }
    }

    function cbb_term_link_shortcode( $atts, $content = null ) {
        $term_link=get_option('ccb_term_link');
        return '<a href="'.$term_link.'" target="_blank">' . $content . '</a>';
    }
    
    function cookie_consent_banner_settings()
    {
        $this->ccb_update_options($_POST);
    
        ?>
        <div class='wrap'>
            <h1>Cookie consent banner settings</h1>
            <form method='post'>
    
            <table class="form-table">
                <tbody>
                    <tr>
                        <th>Message</th>
                        <td>
                            <?php
                                $content = get_option('ccb_content');
                                wp_editor( $content, 'ccb_content', array('media_buttons'=>0, 'textarea_rows'=> '5', 'textarea_name'=> 'ccb_content', 'teeny'=> 1) );
                            ?>
    
                            <?
                                $custom_countries=get_option('ccb_custom_countries');
                                if (empty($custom_countries)):
                                    $cbb_custom_countries_count=0;
                                else:
                                    $cbb_custom_countries_count=count($custom_countries);
                                endif;
                            ?>
    
                            <input 
                            type="button" 
                            class="button-primary" 
                            value="Add message for the visitors from different countries" 
                            id="add_custom_country_message"
                            data-custom-messages-count="<?=$cbb_custom_countries_count?>"
                            >
                            <div class="ccb_different_countries">
                                <? 
                                if (!empty($custom_countries)):
                                    foreach($custom_countries as $custom_country_name => $custom_country_message):
                                        ?>
                                        <div class="ccb_custom_country_fields">
                                        <?
                                        $content = $custom_country_message;
                                        wp_editor( $content, 'ccb_content_'.$custom_country_name, array('media_buttons'=>0, 'textarea_rows'=> '5', 'textarea_name'=> 'ccb_country_message[]', 'teeny'=> 1) );    
                                        ?>
                                            <label for="ccb_country_name">
                                            Select country: 
                                            <select name="ccb_country_name[]">
                                                <? foreach($this->ccb_countries as $ccb_country_code => $ccb_country_name):?>
                                                    <option value="<?=$ccb_country_code?>" <? if ($custom_country_name==$ccb_country_code):?>selected<?endif;?>><?=$ccb_country_name?></option>
                                                <? endforeach; ?>
                                            </select>
                                            </label>
                                        </div>
                                        <?
                                    endforeach;
                                endif;?>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Icon</th>
                        <td>
                            <div class="ccb_icon_uploader">
                                <? 
                                $ccb_icon_id=get_option('ccb_icon');
                                $ccb_icon_url=wp_get_attachment_url($ccb_icon_id);
                                if (empty($ccb_icon_id)) $ccb_icon_url=plugins_url('assets/images/ccb_icon_empty.png' , $this->file);
                                ?>
                                <img id="ccb_icon_preview" src="<?=$ccb_icon_url?>"><br>
                                <input id="upload_image_button" type="button" class="button-primary" value="Change Image" />
                                <input id="ccb_icon" type="hidden" name="ccb_icon" value="<?=$ccb_icon_id;?>" />
                                
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Link to the full version of terms</th>
                        <td>
                            <input type="text" name="ccb_term_link" value="<?php echo get_option('ccb_term_link'); ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Background color</th>
                        <td>
                            <input type="text" class="color-field" name="ccb_bg_color" value="<?php echo get_option('ccb_bg_color'); ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Button color</th>
                        <td>
                            <input type="text" class="color-field" name="ccb_button_color" value="<?php echo get_option('ccb_button_color'); ?>">
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><? submit_button('Update options', 'primary'); ?></td>
                    </tr>
                </tbody>
            </table>
            
            </form>
        </div>
        <?php
    }

    
    
    function ccb_add_country_fields() {
        ?>
        <div class="ccb_custom_country_fields">
        <?
        wp_editor( '', 
            $_POST['textarea_id'], 
            array('media_buttons'=>0, 'textarea_rows'=> '5', 'textarea_name'=> 'ccb_country_message[]') );
        ?>
        <label for="ccb_country_name">
            Select country: 
            <select name="ccb_country_name[]">
                <? foreach($this->ccb_countries as $ccb_country_code => $ccb_country_name):?>
                <option value="<?=$ccb_country_code?>"><?=$ccb_country_name?></option>
                <? endforeach; ?>
            </select>
        </label>
        </div>
        
        <?
        wp_die();
    }

    function ccb_get_visitor_country()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        //visitor country detecting
        $res = file_get_contents('https://www.iplocate.io/api/lookup/'.$ip);
        $res = json_decode($res);

        if (!empty($res->country_code)): return $res->country_code;
        else: return false; endif;
    }


    function cbb_add_consent_baner()
    {
        $ccb_icon_id=get_option('ccb_icon');
        if (empty($ccb_icon_id)):
            $ccb_icon_url=plugins_url('assets/images/ccb_icon_empty.png' , $this->file);
        else:
            $ccb_icon_url=wp_get_attachment_url($ccb_icon_id);
        endif;

        $ccb_content=get_option('ccb_content');

        $visitor_country_code=$this->ccb_get_visitor_country();
        if ($visitor_country_code !== false ):
            $custom_countries=get_option('ccb_custom_countries');
            if (isset($custom_countries[$visitor_country_code])):
                $ccb_content=$custom_countries[$visitor_country_code];
            endif;
        endif;

        $ccb_content=apply_filters('the_content', $ccb_content);

        $ccb_bg_color=get_option('ccb_bg_color');
        $ccb_button_color=get_option('ccb_button_color');
        ?>
        <div class="ccb_popup_cookie" style="background-color:<?=$ccb_bg_color;?>">
            <div class="ccb_popup_cookie_inner">
                <div class="ccb_popup_notification">
                    <div class="cbb_icon">
                        <img src="<?=$ccb_icon_url?>">
                    </div>
                    <?=$ccb_content?>
                    <? print_r($country);?>
                </div>
                <div class="ccb_popup_buttons">
                    <a href="" 
                    class="ccb_button" 
                    id="ccb_cookie_agree" 
                    style="background-color:<?=$ccb_button_color?>; border-color:<?=$ccb_button_color?>; color:<?=$ccb_bg_color; ?>"
                    >Accept</a>
                    <a href="" 
                    class="ccb_button disagree" 
                    id="ccb_cookie_disagree"
                    style="border-color:<?=$ccb_button_color?>; color:<?=$ccb_button_color; ?>"
                    >Close</a>
                </div>
                <div class="ccb_popup_cookie_close"></div>
            </div>
        </div>
        <?
    }
}
?>