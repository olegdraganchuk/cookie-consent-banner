<?

/**
 * Plugin Name: Cookie consent banner
 * Description: Create popup banner about cookie consent
 * Plugin URI:  https://olegdraganchuk.com/cookie-consent-banner
 * Version:     1.0.0
 * Author:      Oleg Draganchuk
 * Author URI:  https://olegdraganchuk.com
 * Text Domain: cookie-consent-banner
 * Domain Path: /languages
 */

require 'classes/Cookie_Consent_Banner_Class.php';

$cookie_consent_banner_class = new Cookie_Consent_Banner_Class(__FILE__);


?>