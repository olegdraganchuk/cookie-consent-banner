jQuery(document).ready(function($){

    var ccb_gdpr=ccb_readCookie('ccb_gdpr');
    if (ccb_gdpr === null)
    {
        $('.ccb_popup_cookie').addClass('show');
    }

    $('#ccb_cookie_agree').on('click', function(e){
        e.preventDefault();
        $('.ccb_popup_cookie').removeClass('show');
        ccb_createCookie('ccb_gdpr', 'agree', 9999) ;
    });

    $('#ccb_cookie_disagree').on('click', function(e){
        e.preventDefault();
        $('.ccb_popup_cookie').removeClass('show');
        ccb_deleteAllCookies();
        ccb_createCookie('ccb_gdpr', 'disagree', 9999) ;
    });

    function ccb_createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = "; expires="+date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function ccb_readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1,c.length);
            }
            if (c.indexOf(nameEQ) === 0) {
                return c.substring(nameEQ.length,c.length);
            }
        }
        return null;
    }

    function ccb_deleteAllCookies() {
        var cookies = document.cookie.split(";");
    
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }


});