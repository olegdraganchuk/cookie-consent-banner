jQuery(document).ready(function($){
    var mediaUploader;

    $('.color-field').wpColorPicker({
        defaultColor: '#66cccc',
        hide: true,
        change: function(event, ui){},
        clear: function(){ },
        });

    $('#add_custom_country_message').on('click', function(){

        var counter=parseInt($(this).attr('data-custom-messages-count'));
        var textarea_id='ccb_content_'+counter;

        var data = {
			action: 'ccb_add_country_fields',
			textarea_id: textarea_id
		};

		jQuery.post( '/wp-admin/admin-ajax.php', data, function(response) {
            $('.ccb_different_countries').prepend(response);
            quicktags({id : textarea_id});
            tinymce.execCommand( 'mceAddEditor', true, textarea_id );
            $('#add_custom_country_message').attr('data-custom-messages-count', counter+1);
		});
    })

    $('#upload_image_button').click(function(e) {
      e.preventDefault();
        if (mediaUploader) {
        mediaUploader.open();
        return;
      }
      mediaUploader = wp.media.frames.file_frame = wp.media({
        title: 'Choose Image',
        button: {
        text: 'Choose Image'
      }, multiple: false });
      mediaUploader.on('select', function() {
        var attachment = mediaUploader.state().get('selection').first().toJSON();
        console.log(attachment);
        $('#ccb_icon').val(attachment.id);
        $('#ccb_icon_preview').attr('src', attachment.url);
      });
      mediaUploader.open();
    });
  });